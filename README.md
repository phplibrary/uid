#SINEVIA UID

Class to generate unique identifying strings

# Installation #

```
#!json

   "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/phplibrary/uid.git"
        }
    ],
    "require": {
        "sinevia/phplibrary/uid": "dev-master"
    },
```

# How to Use? #